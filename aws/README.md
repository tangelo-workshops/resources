# AWS EC2

Como crear una instancia en ec2 con docker y un rol que permita descargar imagenes ecr.

1. Crear un rol ec2-read-ecr y asociar una politica ecr-policy.

```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": [
                "ecr:PutImageTagMutability",
                "ecr:StartImageScan",
                "ecr:ListTagsForResource",
                "ecr:UploadLayerPart",
                "ecr:BatchDeleteImage",
                "ecr:ListImages",
                "ecr:DeleteRepository",
                "ecr:CompleteLayerUpload",
                "ecr:TagResource",
                "ecr:DescribeRepositories",
                "ecr:DeleteRepositoryPolicy",
                "ecr:BatchCheckLayerAvailability",
                "ecr:GetLifecyclePolicy",
                "ecr:PutLifecyclePolicy",
                "ecr:DescribeImageScanFindings",
                "ecr:GetLifecyclePolicyPreview",
                "ecr:CreateRepository",
                "ecr:PutImageScanningConfiguration",
                "ecr:GetDownloadUrlForLayer",
                "ecr:GetAuthorizationToken",
                "ecr:DeleteLifecyclePolicy",
                "ecr:PutImage",
                "ecr:UntagResource",
                "ecr:BatchGetImage",
                "ecr:DescribeImages",
                "ecr:StartLifecyclePolicyPreview",
                "ecr:InitiateLayerUpload",
                "ecr:GetRepositoryPolicy"
            ],
            "Resource": "*"
        }
    ]
}
```

2. Launch instances con Amazon Linux 2 AMI (HVM) 64-bit. Elegir T2-micro.

3. En los detalles de la imagen, seleccionar  IAM Role ec2-read-ecr. Creada en el punto 1.

4. Crear un security group con ssh source, my ip.

5. En Launch, crear(si es necesario) la llave privada para acceder via ssh a la instancia.

6. Para conectarse a la instancia:

```bash
ssh -i private_key ec2-user@public_instance_ip
```

7. Instalar docker y asignar el usuario el grupo de docker.

```bash
yum install docker
```

8. Crear repositorio en ECR test-worker-python

9. Hacer test de pull y push desde la instancia
