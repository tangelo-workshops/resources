# Recursos de tangelo workshop

## Organización y metodologías ágiles
Las organizaciones que actualmente emergen en el mundo 'tech' buscan desarrollar productos de software de manera continua y rápida.
Las metodologías ágiles adaptan el proceso en el cual los productos de software continuamente cumplen su ciclo de vida de forma optima y continua según los requerimientos.

- What is scrum ?
https://www.scrum.org/resources/what-is-scrum

- The Art of Agile Practice: A Composite Approach for Projects and Organizations
https://books.google.com.co/books?id=ZqnMBQAAQBAJ&pg=PA56&redir_esc=y#v=onepage&q&f=false

## Monolitos y microservicios
Generalmente al inicio de un proyecto, se busca consolidar un producto mínimo viable ([MVP](https://en.wikipedia.org/wiki/Minimum_viable_product)). Las aplicaciones monoliticas son facilmente desarrolladas por frameworks que son altamente soportados por la comunidad, su arquitectura y desarrollo son faciles de comprender y son altamente extensibles. Algunos mas conocidos son:

- https://laravel.com/
- https://www.djangoproject.com/
- https://rubyonrails.org/

Las arquitecturas orientadas a microservicios se han vuelto populares y han tenido gran acogida en empresas reconocidas porque es mucho mas facil mantener una gran cantidad de servicios con unicas responsabilidades. Estas aplicaciones necesariamente no estan atadas a un framework especifico, sin embargo, para desarrollar aplicaciones a medida, existen los siguientes frameworks:

- https://falcon.readthedocs.io/en/stable/
- https://www.slimframework.com/
- https://lumen.laravel.com/docs/9.x
- https://github.com/ruby-grape/grape
- https://flask.palletsprojects.com/en/2.0.x/
- https://fastapi.tiangolo.com/

Extra
- https://www.youtube.com/watch?v=CZ3wIuvmHeM&ab_channel=InfoQ
- https://microservices.io/

## Contenedores 
Un contenedor se entiende como una "unidad" de software que es capaz de empaquetar el codigo de software junto con sus dependencias. Esta unidad puede ser ejecutada en cualquier ambiente sin que cambie algo de si mismo.
Estas unidades se denominan imagenes.

*What is a container? by docker*
- https://www.docker.com/resources/what-container

Tutorial en español para manejar docker: 
- https://www.youtube.com/watch?v=CV_Uf3Dq-EU&t=1681s&ab_channel=PeladoNerd

## Api REST

REST se conoce como la arquitectura propuesta por Roy Thomas Fielding en el año 2000. Este estandar controla la manera en la que deben comunicarse las distintas aplicaciones a traves del protocolo HTTP.

Cuando un servicio web adopta la arquitectura REST para exponer datos o comunicarse con otros sistemas se denomina API REST.

- https://www.ics.uci.edu/~fielding/pubs/dissertation/rest_arch_style.htm
- https://www.redhat.com/en/topics/api/what-is-a-rest-api
- https://restfulapi.net/

## Python 
- **Zen de python**
https://www.python.org/dev/peps/pep-0020/
- **Tipos de datos en python**
```python
# String
"this is a string"
# Integer, float, complex
integer_number = 1
float_number = 1.1
complex_number = 1j 
# list, tuple, range
list_object = [1, 2, 3, 'test', 3.4, type()]
tuple_object = (1,2,'3')
range_object = range(6)
# dict
dict_object = {"name" : "John", "age" : 36}
#set, bool
set_object = {"apple", "banana", "cherry"}
bool_object = True || 1
# bytes...
b"Hello"
bytearray(5)
```
- **Tips**
```python
[] es mas rapido que list()
{} es mas rapido que dict()
...
my_list = []
for x in range(10):
	my_list.append(x)
	
es equivalente a 

my_list = [x for x in range(10)]
```
- **Archivos**

Un archivo tiene extension .py y puede llevar metodos, clases o variables declaradas

```python
# my_file.py

def my_method():
	return True

my_var = "this"
```
y se importa ese archivo como
```python
from my_file import my_method
from my_file import my_var

method()
my_var
```
Las importaciones funcionan asi: 
- https://docs.python.org/es/3/reference/import.html

- **Clases en python**
```python
class MyClass:
	# ESTE NO ES EL CONSTRUCTOR
	# pero se puede usar asi por el momento...
	def __init__(self):
		pass
		
	def my_method(self):
		pass
```

- **Paquetes en python**
```bash
# Crear ambiente virtual
python3 -m venv venv

# activar el ambiente, "entrar en el "
source venv/bin/activate
# salir del ambiente
deactivate 

# para usar venv sin entrar
venv/bin/python
venv/bin/pip3 install ...

# Instalar recursivamente requirements
pip3 install -r requirements.txt

# Instalar un paquete con pip
pip3 install celery

# Ver paquetes instalados 
pip3 freeze
```

## Ruby
- **Tipos de datos en ruby**
-   Numbers
```ruby
# float type
distance = 0.1
  
# both integer and float type
time = 9.87 / 3600
```
-   Boolean
```ruby
true
nil
0
1
```
-   Strings
```ruby
'string'
```
-   Hashes
```ruby
{ "red" => 0xf00, "green" => 0x0f0, "blue" => 0x00f }
{ red: 0xf00, green: 0x0f0, blue: 0x00f }
```
-   Arrays
```ruby
[ "fred", 10, 3.14, "This is a string", "last element"]
```
-   Symbols
```ruby
:weird
:data
:type
```
- **Archivos**

Un archivo tiene extension .rb y puede llevar metodos, clases o variables declaradas

```ruby
# my_file.rb

def my_method
	do_something...

def another_method(arg1, arg2)
	do_something...

my_var = "this"
```
y se importa ese archivo como
```ruby
require my_file
require_relative '../my_file'

another_method(12, 34)
my_var
```

- **Clases y modulos en ruby**
```ruby
class MyClass

	def initialize
		do_something
	end
		
	def my_method(args)
		1+1
	end
end

module Utilities

	def util
		2+2 == 5
	end
	
	def Utilities.very_util(obj)
		obj.is_a?(Hash) && obj.key?(:status)
	end
end

class Implement
	use Utilities

	def use_util
		hash_obj = { status: true }
		very_util(hash_obj)
	end
end

def use_util
	hash_obj = { status: true }
	Utilities.very_util(hash_obj)
end
```
